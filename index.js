
// common example of arrays
let grades =[98.5, 94.3, 89.2, 90.1];
let computerBrand =['acer', 'asus', 'lenovo', 'neo', 'redfox', 'gateway', 'toshiba', 'fujitsu'];

// not recommended use of array
let mixedArr =[12, 'asus', null, undefined , {}];

// alternative way to write array
let myTask =[
		'drink HTML',
		'eat javascript',
		'inhale css',
		'bake sass'
		];


// index = n - 1 where n is the total lenght of array

// reassigning array values
console.log("array before reaasigment");
console.log(myTask);
myTask[0] = 'clean node';
console.log('array after reassignment');
console.log(myTask);

// reading from array
console.log("your grade is:" + grades[0]);
console.log(computerBrand[3]);

// accessing an array elements that does not exist
console.log(grades[20]);


// getting the length of an array (.length)
console.log(computerBrand.length);


// manipulate arrays
// array methods
// mutator methods - functions that mutate or chnage an array after they're created


let fruits =['Apple','Orange','Kiwi','Dragon fruit'];

// push() method - it add an element in the end of an array and returns the array's length
console.log("Current Array");
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits);


// adding multiple eleent in an array
fruits.push('avocado', 'Guava');
console.log('Mutated array from push method');
console.log(fruits);


// pop() -removes the last element in an array and returns the removed element
let removeFruit = fruits.pop();
console.log(removeFruit);
console.log('Mutated array from pop method');
console.log(fruits);


// unshift() - it will add 1 ore more eleent at the beginning of an array
/*
syntax
	arrayName=unshift('element');
	arrayName=unshift('elementA', 'elementB');
*/

// let addFruit = fruits.unshift('Lime','Banana');
// console.log(addFruit);
fruits.unshift('Lime','Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

// shift()- removes an element at the beginning of an array and return the remove element

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);


// splice()- simultaneously removes elements from a specified index number and adds element
/* syntax:
	arrayName.splice(startingindex, numberOfElementToDelete, elementToBeAdded);
*/

fruits.splice(1, 2, 'Lime','Cherry')
console.log("Mutated arrray from splice method");
console.log(fruits);



// sort()- rearrange the array elements in alphanumeric order
/*
syntax:
	arrayName.sort();
*/

fruits.sort()
console.log("Mutated arrray from sort method");
console.log(fruits);


// reverse() - reverse the order of array element
/*syntax:
	arrayname.reverse();
	*/
fruits.reverse();
console.log("Mutated arrray from reverse method");
console.log(fruits);


// non-mutator methods - functions that do not modify or change an array after they're created

let countries =['US', 'PH','CAN','SG','TH','PH','FR','DE'];
// indexOf()- return the index number of the first matching element foud in an array.
/*syntax:
	arrayName.indexOf(searchValue);
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry =countries.indexOf('BR');
console.log("Result of indexOf method: " + invalidCountry);


// lastIndexOf() - returns the index number of the last mactching element found in an array
/*syntax:
	arrayname.lastIndexOf(searchValue);
	*/

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);


// slice() portion/slices elements of an array and returns a new array
/*Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex,lenghtcount);
*/
// slicing of elements from a specified index to another index
console.log(countries);
let slicedArrayA = countries.slice(2);
console.log("Result from slice method");
console.log(slicedArrayA);

console.log(countries);
let sliceArrayB = countries.slice(2, 6);
console.log("Result from slice method");
console.log(sliceArrayB);


// toString() - returns an aray as a string seperated by comas.
/*syntax:
	arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from tostring method");
console.log(stringArray);


// concat() - combins two rrays and returns the combined result
/*syntax:
	arrayA.concat(arrayB);
	*/

let taskArrayA = ['drink html','eat javascript'];
let taskArrayB = ['inhale css','breath sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method");
console.log(tasks);


// combining multiple arrays
console.log('Result from concat method');
let alltasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(alltasks);

// combining arrays with elements
let combinedtask = taskArrayA.concat('smell express','throw react');
console.log("Result from concat method");
console.log(combinedtask);


// join()- returns an array as a string seperated by specified sepeartor string
/*syntax:
	arrayName.join('seperatorString');
	*/

let users = ['john','jane','joe','rober'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


// Iteration methods - are loops designed t perform repetitive task on arrays
//  common practice to use the singular form of the array content

// forEcah()- similar to a for loop that iterates on each array element
/*syntax:
	arrayName.forEach(function(individualElement){
	statement;
	})
	*/
console.log('Result from forEach method');
alltasks.forEach(function(task){
	console.log(task);
})

// using forEach with condiional statements

let filteredTasks = [];

alltasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task)
	}
});
console.log('Result of filtered task: ')
console.log(filteredTasks);

// map() iterate on each element and returns a new array with diffirent values depending on the result of the functions operation.
/*syntax
	let/const resultArray = arrayName.map(function(indivElement){
	statements;
	});
	*/

	let numbers = [1,2,3,4,5];
	let numberMap = numbers.map(function(number){
		return number * number;
	})
	console.log('Result of map task: ')
	console.log(numberMap);


// every() checks if all elements in an array meet the given conditon the result is boolean--  false only

/*syntax:
	let/const resultArray = arrayName.every(function(indivElement){
	return expression/condition
	})
*/

let allValid = numbers.every(function(number){
	return(number < 3);
});
console.log('Result of every method');
console.log(allValid);


// some() if atleast one elemet in the array meets the given condition- true value only
/*let/const resultArray = arrayName.some(function(				indivElement){
	return expression/condition
	})
*/

let someValid = numbers.some(function(number){
	return(number < 2);
});
console.log('Result of some method');
console.log(someValid);


// filter() - returns a new array that conatins elements which meets the given condition
/*syntax:
	let/const resultArray = arrayName.filter(funtion(indivElement){
	return exprssion/ expression
	})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log('Result of filter method');
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number == 0);
})
console.log('Result of filter method');
console.log(nothingFound);


// fitering using forEach method
let filteredNumbers =[];

numbers.forEach(function(number){
	if(number > 3){
		filteredNumbers.push(number);
	}
})
console.log('Result of filter method');
console.log(filteredNumbers);


// includes()-the result of the first method is used on the second method untill all chained methods have been resolve


let products =['Mouse','Keyboard','Laptop', 'Monitor'];
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log(filteredProducts);

// reduce()- evaluate elements from left to right and returns/ reduce the array into a single value
/*syntax:
	let/const resultArray=arrayName.reduce(function(accumalator, currentValue){
	return expression/ opeartion
	})
*/

let iteration = 0;
let reduceArray = numbers.reduce(function(x,y){
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	return x + y;
})
console.log("result of reduce method: " + reduceArray);


// Reducing string arrays

let list =['Hello', 'Again', 'World'];
let reducedJoin = list.reduce(function(x,y){
	return x + ' ' + y;
})
console.log("Result of reduce method: " + reducedJoin);



// Multidimentional array
// Two dimensional array- having an array within an array

let oneDim = [];
// 1st dime     0      1
// 2nd dim     0 1    0  1
let twoDim = [[2,4], [6,8]];

// 2x2 Two dimentional array
console.log(twoDim[1][0]);
console.log(twoDim[1][1]);

// 3x2 two dimentional array
//                0      1       2
//              0  1   0  1   0   1
let twoDim2 = [[2, 4],[6, 8],[10, 12]];
console.log(twoDim2[2][0]);


function myName(name){
	console.log('Hello' + name)
}














































/*

let name = prompt("enter your fruit");
function addFruit(){
	fruits.push(name);
}
	addFruit(fruits);
	console.log(fruits);


let index = parseInt(prompt("enter your index number"));
let number = parseInt(prompt("enter the number of element"));
let addIndexFruit = prompt("enter the Fruit");

function spliceFruit(){
	fruits.splice(index,number, addIndexFruit);
}
	spliceFruit(fruits);
	console.log(fruits);

*/








